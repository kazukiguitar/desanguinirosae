\select@language {italian}
\select@language {italian}
\contentsline {chapter}{\numberline {0}Introduzione}{1}{chapter.0}
\contentsline {chapter}{\numberline {1}Indagini a Torino}{3}{chapter.1}
\contentsline {section}{\numberline {1.1}Antefatto}{3}{section.1.1}
\contentsline {section}{\numberline {1.2}Incontro al Duomo}{3}{section.1.2}
\contentsline {section}{\numberline {1.3}Alberto Pastani}{6}{section.1.3}
\contentsline {subsection}{\numberline {1.3.1}Casa di Alberto Pastani}{7}{subsection.1.3.1}
\contentsline {subsection}{\numberline {1.3.2}Studio del Dott. Silvio Ricari}{7}{subsection.1.3.2}
\contentsline {section}{\numberline {1.4}Francesco Moro}{8}{section.1.4}
\contentsline {section}{\numberline {1.5}Pietro Bando}{9}{section.1.5}
\contentsline {subsection}{\numberline {1.5.1}Appartamento di Pietro Bando}{10}{subsection.1.5.1}
\contentsline {subsection}{\numberline {1.5.2}Studio di Federico Pannofino}{10}{subsection.1.5.2}
\contentsline {section}{\numberline {1.6}Il covo dei ROMANUS}{11}{section.1.6}
\contentsline {subsection}{\numberline {1.6.1}La palazzina abbandonata}{11}{subsection.1.6.1}
\contentsline {subsection}{\numberline {1.6.2}Il marchingegno non si muove}{11}{subsection.1.6.2}
\contentsline {subsection}{\numberline {1.6.3}Il consiglio dei 7 Re}{13}{subsection.1.6.3}
\contentsline {section}{\numberline {1.7}L'appuntamento al castello}{15}{section.1.7}
\contentsline {section}{\numberline {1.8}Colloquio con Bagnoli}{19}{section.1.8}
\contentsline {section}{\numberline {1.9}Analisi prove}{19}{section.1.9}
\contentsline {section}{\numberline {1.10}Fine Capitolo 1}{20}{section.1.10}
\contentsline {section}{\numberline {1.11}Allegati}{21}{section.1.11}
\contentsline {subsection}{\numberline {1.11.1}Allegato 1.A - Referto medico Pastani}{21}{subsection.1.11.1}
\contentsline {subsection}{\numberline {1.11.2}Allegato 1.B - Referto medico Bando}{21}{subsection.1.11.2}
\contentsline {subsection}{\numberline {1.11.3}Allegato 1.C - Lettera Millevolti}{21}{subsection.1.11.3}
\contentsline {subsection}{\numberline {1.11.4}Allegato 1.D - Permesso Bagnoli}{22}{subsection.1.11.4}
\contentsline {subsection}{\numberline {1.11.5}Allegato 1.E Battaglia a Rivoli - Pianta}{22}{subsection.1.11.5}
\contentsline {subsection}{\numberline {1.11.6}Allegato 1.F Battaglia a Rivoli - Fronte}{23}{subsection.1.11.6}
